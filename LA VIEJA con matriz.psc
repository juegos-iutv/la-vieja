Algoritmo lavieja
	
	Dimension tab[3,3]	
	
	//hacer que todos valgan un espacio
	Para f<-1 Hasta 3 Hacer
		Para c<-1 Hasta 3 Hacer
			tab[f,c]<-" "
		Fin Para
	Fin Para
	
	ganador<-0
	turno<-0
	
	Repetir
		turno=turno+1
		Limpiar Pantalla
		
		// dibujo tablero
		Escribir "   - 3 EN LINEA -"
		Escribir " "
		Escribir "      |     |     "
		Escribir "   ",Tab[1,1],"  |  ",Tab[1,2],"  |  ",Tab[1,3]
		Escribir "     1|    2|    3"
		Escribir " -----+-----+------"
		Escribir "      |     |     "
		Escribir "   ",Tab[2,1],"  |  ",Tab[2,2],"  |  ",Tab[2,3]
		Escribir "     4|    5|    6"
		Escribir " -----+-----+------"
		Escribir "      |     |     "
		Escribir "   ",Tab[3,1],"  |  ",Tab[3,2],"  |  ",Tab[3,3]
		Escribir "     7|    8|    9"
		Escribir " "
		
		//determina turno
		Si turno es impar Entonces
			Escribir "Turno del Jugador 1 (O)"
			ficha<-"O"
		Sino
			Escribir "Turno del Jugador 2 (X)"
			ficha<-"X"
		FinSi
		
		//guarda posicion valida
		Repetir
			ocupado<-0
			Escribir "Ingrese posicion (1-9)"
			leer pos
			
			//jugadas
			Segun pos Hacer
			1:
				Si tab[1,1]=" " Entonces
					Tab[1,1]<-ficha
				Sino
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			2:
				Si tab[1,2]=" " Entonces
					Tab[1,2]<-ficha
				Sino
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			3:
				Si tab[1,3]=" " Entonces
					Tab[1,3]<-ficha
				Sino
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			4:
				Si tab[2,1]=" " Entonces
					Tab[2,1]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			5:
				Si tab[2,2]=" " Entonces
					Tab[2,2]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			6:
				Si tab[2,3]=" " Entonces
					Tab[2,3]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			7:
				Si tab[3,1]=" " Entonces
					Tab[3,1]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			8:
				Si tab[3,2]=" " Entonces
					Tab[3,2]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
			9:
				Si tab[3,3]=" " Entonces
					Tab[3,3]<-ficha
				Sino 
					Escribir "Posicion ya OCUPADA"
					Escribir " "
					ocupado<-1
				FinSi
				
			De Otro Modo:
				Escribir "Posicion NO VALIDA"
			Fin Segun
		Hasta Que pos>0 y pos<10 y ocupado=0
		
		//determinar si hay 3 en linea
		Si tab[1,1]=ficha y tab[1,2]=ficha y tab[1,3]=ficha Entonces	//F1
			ganador<-1
		FinSi
		
		Si tab[2,1]=ficha y tab[2,2]=ficha y tab[2,3]=ficha Entonces	//F2
			ganador<-1
		FinSi
		
		Si tab[3,1]=ficha y tab[3,2]=ficha y tab[3,3]=ficha Entonces	//F3
			ganador<-1
		FinSi
		
		Si tab[1,1]=ficha y tab[2,1]=ficha y tab[3,1]=ficha Entonces	//C1
			ganador<-1
		FinSi
		
		Si tab[1,2]=ficha y tab[2,2]=ficha y tab[3,2]=ficha Entonces	//C2
			ganador<-1
		FinSi
		
		Si tab[1,3]=ficha y tab[2,3]=ficha y tab[3,3]=ficha Entonces	//C3
			ganador<-1
		FinSi
		
		Si tab[1,1]=ficha y tab[2,2]=ficha y tab[3,3]=ficha Entonces	//D1
			ganador<-1
		FinSi
		
		Si tab[1,3]=ficha y tab[2,2]=ficha y tab[3,1]=ficha Entonces	//D2
			ganador<-1
		FinSi
		
		//determina ganador
		Si ganador=1 Entonces
			
			Limpiar Pantalla
			
			Escribir "   - 3 EN LINEA -"
			Escribir " "
			Escribir "      |     |     "
			Escribir "   ",Tab[1,1],"  |  ",Tab[1,2],"  |  ",Tab[1,3]
			Escribir "     1|    2|    3"
			Escribir " -----+-----+------"
			Escribir "      |     |     "
			Escribir "   ",Tab[2,1],"  |  ",Tab[2,2],"  |  ",Tab[2,3]
			Escribir "     4|    5|    6"
			Escribir " -----+-----+------"
			Escribir "      |     |     "
			Escribir "   ",Tab[3,1],"  |  ",Tab[3,2],"  |  ",Tab[3,3]
			Escribir "     7|    8|    9"
			Escribir " "
			
			Escribir Sin Saltar "Ganador: "
			
			Si Turno es impar Entonces
				Escribir "�Jugador 1!"
			Sino
				Escribir "�Jugador 2!"
			FinSi
		FinSi
		
		//gana la vieja
		Si Turno=9 y ganador=0 Entonces
			
			limpiar pantalla
			
			Escribir "   - 3 EN LINEA -"
			Escribir " "
			Escribir "      |     |     "
			Escribir "   ",Tab[1,1],"  |  ",Tab[1,2],"  |  ",Tab[1,3]
			Escribir "     1|    2|    3"
			Escribir " -----+-----+------"
			Escribir "      |     |     "
			Escribir "   ",Tab[2,1],"  |  ",Tab[2,2],"  |  ",Tab[2,3]
			Escribir "     4|    5|    6"
			Escribir " -----+-----+------"
			Escribir "      |     |     "
			Escribir "   ",Tab[3,1],"  |  ",Tab[3,2],"  |  ",Tab[3,3]
			Escribir "     7|    8|    9"
			Escribir " "
			
			Escribir "�Gana la vieja!"
		FinSi
	Hasta que ganador=1 o turno=9
FinAlgoritmo

//solucionar:
